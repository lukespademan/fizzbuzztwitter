# fizzbuzztwitter
### _plays fizzbuzz with you on twitter_


To use: first go to the twitter developer site and generate some API keys.

make a file in the root of the project called .env and fill it with the content:

    CONSUMER_KEY=<key>
    CONSUMER_SECRET_KEY=<key>
    ACCESS_TOKEN_KEY=<key>
    ACCESS_TOKEN_SECRET=<key>
